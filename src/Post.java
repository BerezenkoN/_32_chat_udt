import java.io.Serializable;


/**
 * Created by user on 08.02.2017.
 */
public class Post implements Serializable {
    private String userId;
    private String text;

    public Post(String userId, String text) {
        this.userId = userId;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUserId() {

        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}


